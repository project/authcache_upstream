sub vcl_recv {

  # set the X-Authcache-Key header
  if (req.http.Cookie ~ "Drupal\.authcache\.key=(.*?)(;.*)?$") {
    # without URL decoding it, it was failing on fastly
    set req.http.X-Authcache-Key = urldecode(re.group.1);
  }

#FASTLY recv

  if (req.method != "HEAD" && req.method != "GET" && req.method != "FASTLYPURGE") {
    return(pass);
  }

  return(lookup);
}

sub vcl_fetch {

  # vary on the X-Authcache-Key
  if (beresp.http.Vary) {

    # Another approach, store original vary as an extension of Cache-Control
    # respecting RFC - https://tools.ietf.org/html/rfc7234#section-5.2
    # See 5.2.3.  Cache Control Extensions
    # This is a bit of hack so that 304 responses have this information
    # in vcl_deliver, a custom HTTP header for this does not work.
    if (beresp.http.Cache-Control) {
      set beresp.http.Cache-Control = beresp.http.Cache-Control  ", ";
    }
    set beresp.http.Cache-Control = beresp.http.Cache-Control {"X-Vary=""} beresp.http.Vary {"""};

    set beresp.http.Vary = beresp.http.Vary ",X-Authcache-Key";
    # Remove an unnecessary Cookie so that we vary only on others
    set beresp.http.Vary = regsub(beresp.http.Vary, "Cookie[, ]*", "");
    # Fix possible trailing commas in the end
    set beresp.http.Vary = regsub(beresp.http.Vary, "[ ]*,[ ]*$", "");
  } else {
    set beresp.http.Vary = "X-Authcache-Key";
  }

  if (req.http.Fastly-Debug) {
    set beresp.http.X-Authcache-Upstream-Debug-Vary = beresp.http.Vary;
    set beresp.http.X-Authcache-Upstream-Debug-Key = req.http.X-Authcache-Key;
  }

#FASTLY fetch

  if ((beresp.status == 500 || beresp.status == 503) && req.restarts < 1 && (req.method == "GET" || req.method == "HEAD")) {
    restart;
  }

  if (req.restarts > 0) {
    set beresp.http.Fastly-Restarts = req.restarts;
  }

  if (beresp.http.Set-Cookie) {
    set req.http.Fastly-Cachetype = "SETCOOKIE";
    return(pass);
  }

  if (beresp.http.Cache-Control ~ "private") {
    set req.http.Fastly-Cachetype = "PRIVATE";
    return(pass);
  }

  if (beresp.status == 500 || beresp.status == 503) {
    set req.http.Fastly-Cachetype = "ERROR";
    set beresp.ttl = 1s;
    set beresp.grace = 5s;
    return(deliver);
  }

  if (beresp.http.Expires || beresp.http.Surrogate-Control ~ "max-age" || beresp.http.Cache-Control ~ "(s-maxage|max-age)") {
    # keep the ttl here
  } else {
    # apply the default ttl
    set beresp.ttl = 3600s;
  }

  return(deliver);
}

sub vcl_hit {
#FASTLY hit

  if (!obj.cacheable) {
    return(pass);
  }
  return(deliver);
}

sub vcl_miss {
#FASTLY miss
  return(fetch);
}

sub vcl_deliver {
  # 304s are a special case, in which only a few headers are copied from the
  # cached object
  # From https://tools.ietf.org/html/rfc7232#section-4.1:
  # The server generating a 304 response MUST generate any of the
  # following header fields that would have been sent in a 200 (OK)
  # response to the same request: Cache-Control, Content-Location, Date,
  # ETag, Expires, and Vary.

  # Restore Original Vary from Cache-Control extension set on vcl_deliver
  if (resp.http.Cache-Control ~ "X-Vary=\x22(.*?)\x22") {
    set resp.http.Vary = re.group.1;
  }

  # Remove the Cache-Control custom extension so as it's not needed upstream
  set resp.http.Cache-Control = regsub(resp.http.Cache-Control, "(?:, )?X-Vary=\x22.*?\x22", "");
  if (!resp.http.Cache-Control) {
    unset resp.http.Cache-Control;
  }


#FASTLY deliver
  return(deliver);
}

sub vcl_error {
#FASTLY error
}

sub vcl_pass {
#FASTLY pass
}

sub vcl_log {
#FASTLY log
}
